from django.urls import path
from .views import index,registro,login_view,perfil




urlpatterns = [
    path('', index,name='inicio'),
    path('login/',login_view, name='login'),
    path('registro/',registro, name='registro'),
    path('profile/',perfil, name='profile'),
]