from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login

from django.db.utils import IntegrityError

#modelos
from django.contrib.auth.models import User


# Create your views here.
def index(request):#vista para el inicio de la página
    return render(request,"modulos/index.html")

def login_view(request):#vista para el inicio de sesión de la página
    if request.method=='POST':
        username = request.POST['usuarioLogin']
        password = request.POST['contraseñaLogin']
        
        user=authenticate(request,username=username,password=password)

        if user:
            login(request,user)
            return render(request,"modulos/index.html",{'username':username})
        else:
            return render(request,'modulos/login.html',{'error':'Usuario o contraseña inconrrecto'})
    
    return render(request,"modulos/login.html")


def registro(request):#vista para el registro de la página
    if request.method =='POST':
        username = request.POST['username']
        nombre = request.POST['nombre']
        apellido = request.POST['apellido']
        email = request.POST['email']
        contraseña = request.POST['contraseña']
        contraseña2 = request.POST['contraseña2']

        if contraseña != contraseña2:
            return render(request,'modulos/registro.html',{'error':'Las contraseñas deben coincidir'})
        
        try:
            userNew = User.objects.create_user(username=username,password=contraseña)
        except IntegrityError:
            return render(request, 'modulos/registro.html', {'error': 'este usuario ya se encuentra registrado'})
        userNew.first_name = nombre
        userNew.last_name = apellido
        userNew.email = email
        userNew.save()

        return redirect('login')



    return render(request,"modulos/registro.html")


def perfil(request):
    return render(request,"modulos/profile.html")